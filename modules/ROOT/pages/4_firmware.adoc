<<<
ifeval::["{backend}" == "html5"]
:nofooter:
:docinfo: shared-footer
:leveloffset: +1
endif::[]
:xrefstyle: short
:doc-part: 4
ifeval::["{backend}" == "pdf"]
:!figure:
{counter:figure:0}
:!table:
{counter:table:0}
endif::[]
:numbering:
[#sec:firmware]
= Firmware Development
== Firmware repository
The https://gitlab.cern.ch/atlas-tdaq-felix/firmware/[FELIX firmware GIT repository] is the central place for firmware development.

* The development for phase I has mostly stabilized, and very little new features are added; mostly bugfixes.
** the https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/tree/master[master] branch is protected.
** A new feature or bugfix must be related to a https://its.cern.ch/jira/projects/FLX/[JIRA ticket], the name of the branch should start with the JIRA number. If for example the JIRA ticket is FLX-1354, the feature branch name could be FLX-1354_NSW_640Mb_ELink
** To merge changes into master, a merge request must be created in https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/merge_requests[Gitlab]. The merge request must be assigned to the librarian (Frans Schreuder)
* Development for phase II is very active
** the https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/tree/phase2/master[phase2/master] branch is protected.
** A new feature or bugfix must be related to a https://its.cern.ch/jira/projects/FLX/[JIRA ticket], the name of the branch should start with the phase2/JIRA number. If for example the JIRA ticket is FLX-1354, the feature branch name could be phase2/FLX-1354_NSW_640Mb_ELink
** To merge changes into phase2/master, a merge request must be created in https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/merge_requests[Gitlab]. The merge request must be assigned to the librarian (Frans Schreuder), don't forget to set phase2/master as the target branch.

=== Merging and Validation of Changes
For a merge request to be completed, a few conditions must be met:

* The https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/pipelines[Gitlab CI pipeline] must be completed without issues. This includes simulation and build of the different flavours
* A test on hardware is required (This is now a hard condition for phase1 branches, at a later stage it will also be required for phase2). This test must include a standard set of automated tests, but a newly added feature or set will be tested separately, depending on the feature.
* The changes will be reviewed by the librarian (Frans Schreuder) visually, and comments / questions can be made.

== Sharing and distribution of bitfiles
Bitfiles that are built by Gitlab CI on a merge request or manual trigger are automatically copied to /eos/project/f/felix/www/dev/dist/firmware/Bitfiles_nightly and are accessible here: https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/dist/firmware/Bitfiles_nightly/?C=M;O=D[https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/dist/firmware/Bitfiles_nightly/]

Developers who build a bitfile for a certain purpose can share this among developers or to users through the cerbox user interface:

. Request access to the e-group https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10410323[atlas-tdaq-felix-cernbox-writers]
. To share with other developers, upload a bitfile to https://cernbox.cern.ch/index.php/apps/files/?dir=/__myprojects/felix/cernbox/Bitfiles_development&[cernbox/Bitfiles_development]
** The file will be accessible on https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/dist/firmware/Bitfiles_development/?C=M;O=D[https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/dist/firmware/Bitfiles_development/]
. To share with users, upload a bitfile to https://cernbox.cern.ch/index.php/apps/files/?dir=/__myprojects/felix/cernbox/Bitfiles_development_user&[cernbox/Bitfiles_development_user]
** The file will be accessible on https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/firmware/Bitfiles_development/?C=M;O=D[https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/firmware/Bitfiles_development/]

== Overview of Firmware Modules
The different HDL files / entities are best described in the https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/docs/FELIX_Phase2_firmware_specs.pdf[FELIX Phase2 firmware specification]

== Firmware Top Level
For phase1 based branches, the toplevel VHDL file is dependent on the firmware flavour (GBT / FULL) and the hardware (VC709, BNL712). Note that the BNL712 (FLX712) is version 2.0 of the BNL711, so some names of files and constraints contain bnl711 rather than bnl712.

[cols="1,1,2"]
|===
|Flavour
|Hardware
|Toplevel

|GBT
|VC709
|https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/blob/master/sources/FelixTop/felix_top.vhd[sources/FelixTop/felix_top.vhd]

|FULL
|VC709
|https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/blob/master/sources/FelixTop/felix_fullmode_top.vhd[sources/FelixTop/felix_fullmode_top.vhd]

|GBT
|BNL712
|https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/blob/master/sources/FelixTop/felix_top_bnl711.vhd[sources/FelixTop/felix_top_bnl711.vhd]

|FULL
|BNL712
|https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/blob/master/sources/FelixTop/felix_fullmode_top_bnl711.vhd[sources/FelixTop/felix_fullmode_top_bnl711.vhd]
|===

For phase2 based branches, all flavours are contained in a single toplevel VHDL file:
[cols="1,1,2"]
|===
|Flavour
|Hardware
|Toplevel

|GBT, FULL, PIXEL, STRIP, LPGBT, ...
|VC709, BNL712, VCU128, VMK180
|https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/blob/phase2/master/sources/FelixTop/felix_top.vhd[sources/FelixTop/felix_top.vhd]
|===

== Register Map & JINJA
The PCIe DMA core (Wupper), see also the https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/docs/FELIX_Phase2_firmware_specs.pdf[FELIX Phase2 firmware specification] consists of a DMA engine, but also contains a register map to control, configure and monitor anything in the FELIX firmware. The registers are stored in a YAML file, for phase I branches the version is https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/blob/master/sources/templates/registers-4.10.yaml[rm-4.10], for phase II branches we use https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/blob/phase2/master/sources/templates/registers-5.0.yaml[rm-5.0].

The YAML file has 3 sorts of registers/bitfields:

* R: A read only register, used to monitor status of the firmware
* W: A read/write register, used to control settings.
* T: Trigger: A self clearing bitfield within a register, will pulse shortly (~5 clock cycles) if any bitfield within a register is written

=== Control and Monitor Records
Read only registers are placed in register_map_monitor (internal to Wupper), but it is more practical to drive the status signals from within the different blocks in the firmware, rather than a central point. Therefore the record register_map_monitor is divided into different subrecords called "Monitor Sections". The monitor sections are difined in the top section of the YAML file. All R bitfields must be contained in a monitor section.

Read/Write and Trigger registers are placed in register_map_control records. This record is not divided into sub records as it is easy to fan out the complete register map to where it is required without driver conflicts.

=== Syntax
The syntax of the yaml file is described in the https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/docs/wuppercodegen/latex/WUPPERCodeGenerator.pdf[WupperCodeGen manual], but it is easiest to browse existing registers and copy / paste snippets in order to add a register.

Registers can contain complex structures:

* Single (unnamed) bitfields, resulting in a single register with a std_logic_vector
* Multiple named bitfields, resulting in a record containing multiple std_logic_vector bitfields
* A 1D array of registers, resulting in an array of records.
* A 2D array of registers, resulting in an array of an array of records. For Arrays, references (ref:) in YAML must be used, and the referenced tree must have the keyword "number:" for the size of the array.

=== Generating the files from yaml
To generate the VHDL files (and Latex / HTML documents) from the YAML files, WupperCodeGen is required. WupperCodeGen is depending on: the following system packages:

* python-jinja2
* python-argparse
* python-yaml
* python-markupsafe

It is expected that the different repositories are cloned in the following format:
[listing]
----
felix
├── firmware
└── software
    ├── wuppercodegen
    └── ...
----

This tree can be obtained by cloning the felix repository with the following commands:
[listing]
----
git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/felix
cd felix/
./clone_all.sh ssh
cd software/
./clone_all.sh ssh
----

To build the VHDL files and Latex document, the following commands can be used:

[listing]
----
cd firmware/sources/templates
./build.sh
./build-doc.sh
----


== Getting Started with Vivado
The FELIX Phase I firmware is built with Vivado 2020.1. For CERN Linux computers, an installation on AFS is available, other institutes / systems must manage their own installations.
The FELIX Phase I firmware is build using different vivado versions depending on the hardware:

* The FLX709 and FLX712 builds use Vivado 2021.2
* The FLX182 builds use Vivado 2022.2
* The FLX155 builds use Vivado 2023.2

When Versal Premium is well supported in a future Vivado version in 2024, all the hardware platforms for phase II will be upgraded to that Vivado version.

[listing]
----
export XILINXD_LICENSE_FILE="2112@licenxilinx"
#For Phase I
source /afs/cern.ch/work/f/fschreud/public/Xilinx/Vivado/2020.1/settings64.sh
#For Phase II FLX709 or FLX712
source /eos/project/f/felix/xilinx/Vivado/2021.2/settings64.sh
#For Phase II FLX182
source /eos/project/f/felix/xilinx/Vivado/2022.2/settings64.sh
#For Phase II FLX155
source /eos/project/f/felix/xilinx/Vivado/2024.2/settings64.sh

vivado
----

FELIX is a large project, and therefore needs a solid build server. 64GB of memory or more is recommended to build. Expect build times up to 10 hours for full featured firmware builds.

=== Introduction to FELIX Firmware build scripts
The FELIX build system is based on .tcl scripts, which can be used for Vivado, Questasim (Modelsim) and Sigasi.

==== Creating the Vivado project and building a bitfile
Launch Vivado, and open the TCL console.

To create and build the Vivado project (Phase I branches), type:
[listing]
----
#For GBT mode go to the FELIX_top directory, for FULL mode it is FELIX_fullmode_top
cd felix/firmware/scripts/FELIX_top
#To create the project, replace FLX712 with FLX709 when building for the VC709 card
source ./FLX712_GBT_import_vivado.tcl
#To run synthesis, implementation and create a bitstream in felix/firmware/output
source ./do_implementation_BNL712.tcl
----

To create and build the Vivado project (Phase II branches), type:
[listing]
----
cd felix/firmware/scripts/FELIX_top
#To create the project, replace FLX712 with FLX709 when building for the VC709 card
source ./FLX712_FELIX_import_vivado.tcl
#To run synthesis, implementation and create a bitstream in felix/firmware/output
source ./do_implementation_BNL712_GBT.tcl #GBT mode
source ./do_implementation_BNL712_FULL.tcl #FULL mode
#There are more flavours in that directory that can be built with similar scripts
----

Generated files (.bit, .mcs, .txt, .ltx, .xlsx) will be generated and bundled as .tar.gz in the output/ directory.

==== Debugging with ILA Chipscope probes
An ILA or VIO IP core can simply be added at source level as an IP core, but it can also be added to a syntesized project in the following way:

* In the do_implementation_XXX.tcl script add the following line:

[listing]
set STOP_TO_ADD_ILA 0

* Run the script wand wait for synthesis to be completed. In Vivado open the schematic or netlist view to mark nets as "DEBUG". Then click "Set up debug" under Synthesized Design, to create debug probes. Don't forget to save the synthesized design. Debug probes will be added in constraints/felix_probes.xdc
* in the .tcl console type

[listing]
source ../helper/do_implementation_finish.tcl

* The .bit file and debug_probes.ltx file will be generated in output/
* To start with a clean (no debug) project, empty constraints/felix_probes.xdc

==== Filesets
All the files that are used for building or simulating are defined in filesets. The actual project is created by scripts in the scripts/helper directory. A project specific script for Vivado, Questasim or Sigasi contains the following lines:
[listing]
----
#Initialize empty TCL variables
source ../helper/clear_filesets.tcl

set PROJECT_NAME FLX709_FELIX
set BOARD_TYPE 709
set TOPLEVEL felix_top

#Import blocks for different filesets
source ../filesets/wupper_fileset.tcl
source ../filesets/lpgbt_core_fileset.tcl

#Actually execute all the filesets for Vivado specific
source ../helper/vivado_import_generic.tcl
#For questasim and Sigasi, other tool specific scripts are available in scripts/helper
----

A fileset is a .tcl script that defines TCL arrays using the following syntax:

[listing]
----
set VHDL_FILES [concat $VHDL_FILES \
  templates/pcie_package.vhd \
  templates/dma_control.vhd]
----

In the snippet above, the variable VHDL files is used. Instead of VHDL_FILES, the following variables can be used:
[cols="25,45,35"]
|===
|Variable
|Description
|Relative to

|XCI_FILES
|Xilinx IP core file
|sources/ip_cores/<architecture>

|VHDL_FILES
|Synthesizable VHDL
|sources/

|VERILOG_FILES
|Synthesizable Verilog
|sources/

|SIM_FILES
|VHDL files for simulation
|simulation/

|EXCLUDE_SIM_FILES
|Sythesizable VHDL files, not to be included in simulation
|sources

|WCFG_FILES
|(Deprecated) Vivado waveforms
|simulation/

|BD_FILES
|Vivado Block design
|sources/ip_cores/<architecture>

|XCI_FILES_V7
|IP core files for Virtex7
|sources/ip_cores/<architecture>

|VHDL_FILES_V7
|Synthesizable VHDL for Virtex7
|sources/

|SIM_FILES_V7
|Simulation VHDL for Virtex7
|simulation/

|BD_FILES_V7
|Vivado block design files for Vertex7
|sources/ip_cores/<architecture>

|XCI_FILES_KU
|IP core files for Kintex Ultrascale
|sources/ip_cores/<architecture>

|VHDL_FILES_KU
|Synthesizable VHDL for Kintex Ultrascale
|sources/

|SIM_FILES_KU
|Simulation VHDL for Kintex Ultrascale
|simulation


|BD_FILES_KU
|Vivado block design files for Kintex Ultrascale
|sources/ip_cores/<architecture>

|XCI_FILES_VU9P
|IP Core files for Virtex Ultrascale+ VU9P
|sources/ip_cores/<architecture>

|VHDL_FILES_VU9P
|Synthesizable VHDL for Virtex Ultrascale+ VU9P
|sources/

|SIM_FILES_VU9P
|Simulation VHDL for for Virtex Ultrascale+ VU9P
|simulation/

|BD_FILES_VU9P
|Vivado block design files for Virtex Ultrascale+ VU9P
|sources/ip_cores/<architecture>

|XCI_FILES_VU37P
|IP Core files for Virtex Ultrascale+ VU37P
|sources/ip_cores/<architecture>

|VHDL_FILES_VU37P
|Synthesizable VHDL for Virtex Ultrascale+ VU37P
|sources/

|SIM_FILES_VU37P
|Simulation VHDL for for Virtex Ultrascale+ VU37P
|simulation/

|BD_FILES_VU37P
|Vivado block design files for Virtex Ultrascale+ VU37P
|sources/ip_cores/<architecture>

|XCI_FILES_VERSAL
|IP Core files for Versal Prime
|sources/ip_cores/<architecture>

|VHDL_FILES_VERSAL
|Synthesizable VHDL for Versal Prime
|sources/

|SIM_FILES_VERSAL
|Simulation VHDL for for Versal Prime
|simulation/

|BD_FILES_VERSAL
|Vivado block design files for Versal Prime
|sources/ip_cores/<architecture>

|XDC_FILES_VC709
|Constraints for VC709
|constraints/

|XDC_FILES_HTG710
|Constraints for HTG710
|constraints/

|XDC_FILES_BNL711
|Constraints for BNL711
|constraints/

|XDC_FILES_BNL712
|Constraints for BNL712
|constraints/

|XDC_FILES_VCU128
|Constraints for VCU128
|constraints/

|XDC_FILES_XUPP3R_VU9P
|Constraints for XUPP3R_VU9P
|constraints/

|XDC_FILES_BNL801
|Constraints for BNL VU9P Development board
|constraints/

|XDC_FILES_VMK180
|Constraints for VMK180
|constraints/

|===

==== Helper scripts
The directory scripts/helper contains a set of scripts that should be sourced by other scripts as mentioned above. The different helper scripts are:
[cols="1,3"]
|===
|clear_filesets.tcl
|Initialize all the filesets variables to "" before filesets can be sourced

|vivado_import_generic.tcl
|Create the Vivado project

|questa_import_generic.tcl
|Create the Questasim / Modelsim project

|sigasi_import_generic.tcl
|Create a Sigasi project

|do_implementation_pre.tcl
|A set of tasks before synthesis, call at the beginning of the do_implementation* script

|do_implementation_post.tcl
|Will run synthesis. call at the end of the do_implementation* script.

|do_implementation_finish.tcl
|Called automatically by do_implementation_post.tcl, or in case of ILA probes manually. Will run implementation, bitstream, reports etc.

|===


== Simulation & UVVM

The FELIX firmware is verified per functional block, not as an entire design. There are several testbenches that can be found in the repository.

As a simulation library, UVVM is used to help with utilities and functional models. VUnit is used to streamline the simulation process.

The VUnit simulation has some dependencies:

* Vivado 2021.2, assumed to be installed in /opt/Xilinx/Vivado/2021.2/
* Questasim 2019.1, assumed to be installed in /opt/questasim-2019.1/
* Instead of questasim, you can also use modelsim or GHDL. Some simulations will fail with GHDL due to the encrypted IP of transceivers
* The tcl filesets are loaded in VUnit using python3-tkinter. The package `python3-tkinter` must be installed on the system
* VUnit can be installed by running

[listing]
----
pip3 install --user vunit-hdl
----

To run a certain testbench
[listing]
----
#Source Vivado settings:
~/felix/firmware/simulation/VUnit$ source /opt/Xilinx/Vivado/2021.2/settings64.sh
#Add questasim to the path:
~/felix/firmware/simulation/VUnit$ export PATH=$PATH:/opt/questasim-2019.1/bin
#List available simulations in VUnit
~/felix/firmware/simulation/VUnit$ ./run.py -l
lib.ltittc_vunit_tb.all
lib.lpgbtlinktohost_vunit_tb.all
lib.tb_lcb_command_decoder_vunit.all
lib.decegroup_8b10b_vunit_tb.all
lib.tb_r3l1_regmap_vunit.all
lib.wuppergen4_vunit_tb.all
lib.tb_r3l1_frame_generator_vunit.all
lib.encodingepath_vunit_tb.all
lib.tb_amac_deglitcher_vunit.all
lib.crtohost_vunit_tb.all
lib.ttc_lti_transmitter_vunit_tb.all
lib.busyvirtualelink_vunit_tb.all
lib.decodingpixel_vunit_tb.all
lib.i2c_vunit_tb.all
lib.tb_lcb_regmap_vunit.all
lib.tb_trickle_trigger_vunit.all
lib.tb_lcb_axi_encoder_vunit.all
lib.hgtd_fastcmd_vunit_tb.all
lib.tb_r3l1_scheduler_encoder_vunit.all
lib.crc20_vunit_tb.all
lib.ttctohostvirtualelink_vunit_tb.all
lib.validate_8b10b_vunit_tb.all
lib.crfromhost_vunit_tb.all
lib.loopback25g_vunit_tb.all
lib.tb_lcb_frame_generator_vunit.all
lib.gbtcrcoding_vunit_tb.all
lib.amac_demo_vunit_tb.all
lib.gbtlinktohost_vunit_tb.all
lib.tb_lcb_scheduler_encoder_vunit.all
lib.tb_r3l1_axi_encoder_vunit.all
lib.fullmodetohost_vunit_tb.all
lib.tb_bypass_frame_vvc_vunit.all
lib.tb_strips_configuration_decoder_vunit.all
lib.tb_r3l1_frame_synchronizer_vunit.all
lib.tb_amac_encoder_vunit.all
lib.decodinggearbox_vunit_tb.all
lib.tb_playback_controller_vunit.all
lib.tb_amac_decoder_vunit.all
lib.tb_l0a_frame_generator_vunit.all
lib.wupper_vunit_tb.all
lib.tb_bypass_scheduler_continuous_write_vunit.all
Listed 41 tests
#Run the simulation of your choice in GUI mode
~/felix/firmware/simulation/VUnit$ ./run.py lib.wupper_vunit_tb.all --gui
----

The UVVM testbenches can be found in `firmware/simulation/UVVMtests/tb` (although some testbenches can also be found at other places).

VUnit uses separate testbenches which are simple wrappers around the UVVM testbenches, they can be found in `firmware/simulation/VUnit/tb`
