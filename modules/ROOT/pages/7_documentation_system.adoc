<<<
ifeval::["{backend}" == "html5"]
:nofooter:
:docinfo: shared-footer
:leveloffset: +1
endif::[]
:xrefstyle: short
:doc-part: 7
ifeval::["{backend}" == "pdf"]
:!figure:
{counter:figure:0}
:!table:
{counter:table:0}
endif::[]
:numbering:
[#sec:documentation_system]
= Documentation System

The FELIX documentation system uses a set of converters to publish both an integrated website as well as individual PDF documents.

== Sources and Formats

The (current) FELIX software and some firmware documentation consists of a number of manuals,
which are all in separate gitlab projects:

- The Software Specification: https://gitlab.cern.ch/atlas-tdaq-felix/felix-software-doc
- The User Manual: https://gitlab.cern.ch/atlas-tdaq-felix-dev/felix-user-manual
- The Developer Manual: https://gitlab.cern.ch/atlas-tdaq-felix-dev/felix-developer-manual

Each of these manuals are written in AsciiDoc format (https://asciidoc.org) and their files are organized to be handled
by the Antora (https://antora.org) tool.

Chapters are written file by file. Images of different formats can be included. HTML and PDF are generated.


== Conversion

The actual conversion of AsciiDoc is done by AsciiDoctor (https://asciidoctor.org) and produces html.
A similar AsciiDoctor-pdf tool (https://docs.asciidoctor.org/pdf-converter/latest/) converts each manual to a single PDF file.

Antora orchestrates the actual conversion. To convert locally to HTML you need an installation of
NPM, Antora and LUNR:

```
npm install --global @antora/cli @antora/site-generator @antora/site-generator-default git-describe
npm install @antora/lunr-extension tar-stream fs-extra
```

To convert locally to PDF you need an installation of
Ruby and ascidoctor-pdf:

```
gem install asciidoctor asciidoctor-pdf
```

You can also use FELIX's images which includes these tools:

- NPM, Antora, LUNR and asciidoctor: gitlab-registry.cern.ch/atlas-tdaq-felix/gitlab_ci_runner:antora3-lunr-ext
- Ruby and asciidoctor-pdf: gitlab-registry.cern.ch/atlas-tdaq-felix/gitlab_ci_runner:ruby-asciidoctor

You can then run:

```
./make-html.sh
./make-pdf.sh
```

which produces the website for the current checked out files of the manual in

- build/site/index.html

including an attached PDF file, unless you did not run make-pdf.sh


If files are committed and pushed to the repository the CI will run the conversions for you and
the output of for instance "felix-developer-manual" branch "master" can be found on:

- https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/manuals/felix-developer-manual/master

== Branches and Tags

There is no concept of a staged manual. If you make changes to the repository and push them they get converted
and published. Branches such as 5.x and 4.2.x have therefore the latest documentation. If conversion fails the
previous website and document is still available.

If you want to check out changes, create a branch and push it. It will them be available under:

- https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/manuals/<manual_name>/<branch>/

If you need to publish a fixed version of the manual, run:

```
./make-tag.sh
```

which sets up the antora files for the tag (version), commits and tags it, resets the antora files back to the branch (e.g. 1.x) and commits it again. All of this gets pushed.

 This tag is then published under:

- https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/manuals/<manual_name>/<tag>/

The URLs used here are normally not used by the FELIX users as they would prefer access to the integrated documentation, see below.


== Integration

Antora can integrate several manuals and versions of them into a single website. In the bottom left corner
of each of the pages you can switch between manuals and their versions. Antora creates this site by accessing
each of the gitlab repositories of each of the manuals. It therefore has access to all versions of each manual.
Each manual has its own PDF (see below), however there is no integrated PDF of all manuals.

The integration project itself is on gitlab under:

- https://gitlab.cern.ch/atlas-tdaq-felix-dev/felix-doc

You can convert this locally (you need antora) by running:

```
./make-html.sh
```

which produces the integrated website under:

- build/site/index.html

If files are committed and pushed to the repository the CI will run the conversion for you and
the integrated output can be found on:

- https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-doc

which is the official FELIX website for the manuals.

The CI felix-doc project is triggered every time there is a commit to itself or to any of its manuals.


== PDF

Antora does not know about PDF, therefore the asciidoctor-pdf converter is run for each manual and version individually
at the time of commit/push to the repository. The PDF files are stored for each manual and version combination in:

- https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/pdfs/

When antora builds the fully integrated website it downloads the earlier generated PDFs and attaches them to the
integrated website.


== Extensions

To enable Antora to handle its conversion of individual manuals and integration of them we use a number of extensions.
These (if used) have to be declared in the antora-playbook.xml file in the same order as listed below.

=== Search extension

To enable webwide search for individual manuals as well as for the integrated website you need to include:

- @antora/lunr-extension

This is a generic extension of antora and does not need to be a subproject.

=== Check Config extension

To make sure a subproject has the proper branch/tag and is also referred to as such in the integration project we
check the configuration with this extension. Any misconfigurations are given as errors/warnings.

This ANTORA extension needs to be a subproject of the manual (and of felix-doc). Further info under:

- https://gitlab.cern.ch/antora/check-config-extension


=== Set Version extension

This runs either "git describe" on the repo to get an accurate version number, or in the case of felix-doc
dowloads the "same" version number from a file from the PDFs directory:

- https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/pdfs/

The file is version.adoc and can thus be included in the generated website and PDF.

This ANTORA extension needs to be a subproject of the manual (and of felix-doc). Further info under:

- https://gitlab.cern.ch/antora/set-version-extension


=== Custom Format extension (Numbering sections)

By default antora handles AsciiDoc files without any section/subsection numbering. The antora numbering restarts for
each AsciiDoc file conversion, and thus is wrong. PDF does not have this problem as it regards the AsciiDoc manual
as one file.

This extension numbers the sections and subsections file by file, where each file just needs the chapter number set.

This ANTORA extension needs to be a subproject of the manual (and of felix-doc). Further info under:

- https://gitlab.cern.ch/antora/custom-format-extension


=== Helpfile extension

To include help output from FELIX commands in the documentation we access some files which contains those help text files
and which are created as part of the felix-distribution.

The FELIX command needs to be included in build-tar.sh of felix-distribution. The command also needs
to be included in the build-help.sh. It will then end up in a file like this

- felix-master-rm5-x86_64-centos7-gcc11-opt-help.tar.gz

on the FELIX distribution site:

- https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/latest/

The Helpfile extension downloads such a file, extracts all the help files from it and adds them as virtual files for Antora.
In the AsciiDoc sources of the manual one can then include the help files one by one.

This ANTORA extension needs to be a subproject of the manual (and of felix-doc). Further info under:

- https://gitlab.cern.ch/antora/helpfile-extension


=== Export Content extension

To convert to pdf real AsciiDoc files are needed. The above extensions have generated a bunch of virtual files
(help, version, ...) in the virtual files system of Antora.

This extension exports all files (and renames a few) to a directory to be able to run AsciiDoctor-pdf on.

This ANTORA extension needs to be a subproject of the manual (and of felix-doc). Further info under:

- https://gitlab.cern.ch/antora/export-content-extension


=== PDF Download extension

As the full Antora integration module (felix-doc) can only assemble the html for the website, the previously
uploaded PDF files need to be downloaded again when the integrated site is created.

This extensions downloads any PDFs needed from:

- https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/pdfs/

This ANTORA extension needs to be a subproject of felix-doc. Further info under:

- https://gitlab.cern.ch/antora/pdf-download-extension
