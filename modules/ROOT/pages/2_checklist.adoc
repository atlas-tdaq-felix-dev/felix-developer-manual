<<<
ifeval::["{backend}" == "html5"]
:nofooter:
:docinfo: shared-footer
:leveloffset: +1
endif::[]
:xrefstyle: short
:doc-part: 2
ifeval::["{backend}" == "pdf"]
:!figure:
{counter:figure:0}
:!table:
{counter:table:0}
endif::[]
:numbering:
[#sec:checklist]
= Checklist for new Developers

== Overview

Welcome to the team!

All new developers should follow the following checklist when joining the project:

* Register for the FELIX developer link:https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do[E-group] (needed to view this page on the web!)
* Add the weekly developer meeting to your calendar
** Mondays at 3pm CERN time
** All FELIX meetings can be found in link:https://indico.cern.ch/category/5501/[indico]
* If interested in firmware - add the bi-weekly firmware meeting to your calendar
** Tuesdays at 4pm CERN time
* Review this guide for an introduction to the project and our working model
* Join Mattermost using link:https://mattermost.web.cern.ch/signup_user_complete/?id=ihm5gh55z7867mqqzmm3i7kqhh[this link]! (Note: this replaces the old Slack channels).
* Confirm you have read access to the project CERNbox area: `/eos/project/f/felix` or visible link:https://cernbox.cern.ch/index.php/apps/files/?dir=/__myprojects/felix&[via your browser]
** If you need write access, join the CERNbox access control e-group: `atlas-tdaq-felix-cernbox-writers`

IMPORTANT: We no longer make use of Sharepoint, with all functionality moved to CERNbox. Therefore please don't request access to Sharepoint or direct any users to it.
