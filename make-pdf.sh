#!/bin/sh
set -e

if [ $# -ge 1 ]; then
  BRANCH=${1}
  shift
else
  BRANCH=`git rev-parse --abbrev-ref HEAD`
fi

PROJECT=`sed -n 's/^name: //p' antora.yml`

# build html and copy and make sure we have help files locally for pdf generation Cc_export
antora --extension=Cc_export antora-playbook.yml --stacktrace --log-level debug --log-failure-level error

# generate pdf
# mkdir -p build/site/${PROJECT}/${BRANCH}/_attachments
mkdir -p build/site/${PROJECT}/_attachments
# asciidoctor-pdf --failure-level ERROR --warnings Cc/modules/ROOT/pages/${PROJECT}.adoc  -o build/site/${PROJECT}/${BRANCH}/_attachments/${PROJECT}.pdf
asciidoctor-pdf --failure-level ERROR --warnings Cc/modules/ROOT/pages/${PROJECT}.adoc  -o build/site/${PROJECT}/_attachments/${PROJECT}.pdf
find build
